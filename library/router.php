<?php
session_start(); //session start
$controller; $function; $loader;

function __autoload($className) {
	$file;
	//All classes are called by controller will be processed in this procedure
	$res = explode('_' , $className);
	$filename = $res[0]; $suffix = $res[1];
	if (strtolower($className) == "sec_controller") //SEC_Controller extends handling
		$file = SERVER_ROOT . '/library/controller.php';
	else if (strtolower($className) == "sec_model") //SEC_Model extends handling
		$file = SERVER_ROOT . '/library/model.php';
	else if (strtolower($suffix) == "database") //new Database handling
		$file = SERVER_ROOT . '/library/mysql.php';
	else if (strtolower($className) == "sec_view") //SEC_View extends handling
		$file = SERVER_ROOT . '/library/view.php';
	else if (strtolower($suffix) == "model") //Calling model handling
		$file = SERVER_ROOT. "/model/".strtolower($filename).".php";
	else die("Something wrong happen");

	if (file_exists($file)) include_once($file); //include once
	else die("Class ".$file." is not found");
}

/* Getting route from URL */
$URI = $_SERVER['REQUEST_URI'];
$parsed = explode('/' , $URI);
$controller = $parsed[3];
$func = $parsed[4];
$parsingfunc = explode('?',$func);
$function = $parsingfunc[0];
/* End getting */

/* Getting all data */
$getVars = array();
foreach ($_REQUEST as $key => $value) {
	$getVars[$key] = $value;
}
/* End of getting all data */


/* Opening controller function */
$target = SERVER_ROOT . '/controller/'.$controller.'.php';
if (file_exists($target))
{
	include_once($target);
	$class = ucfirst($controller);

	if (class_exists($class)) $loader = new $class(); //instansiasi kelas
	else die('class '.$class.' is not found');
}
else die('Controller file '.$class.'.php is not found');

//Call Function
$loader->$function($getVars); 
