<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('SERVER_ROOT' , getcwd()."/");
require_once(SERVER_ROOT."/config/database.php");
require_once(SERVER_ROOT."/config/config.php");
require_once(SERVER_ROOT."/library/router.php");
